module "network" {
	#source = "git::https://gitlab.com/hashicorp-demo/hashicorp//network?ref=v1.2.0"
	#source	= "git::https://gitlab.com/hashicorp-demo/hashicorp//network"
	name = "hashi-demo-dev"
	environment = "dev"
	region = "us-east-1"
	azs = ["us-east-1a", "us-east-1b", "us-east-1c", "us-east-1d"]
	cidr_block = "10.10.8.0/21"
	private_ranges = ["10.10.8.0/24", "10.10.9.0/24", "10.10.10.0/24", "10.10.11.0/24"]
	public_ranges = ["10.10.12.0/24", "10.10.13.0/24", "10.10.14.0/24", "10.10.15.0/24"]
	private_sn = "true"
}

module "stack" {
	#source = "git::https://gitlab.com/hashicorp-demo/hashicorp//stack?ref=v1.2.0"
	source = "git::https://gitlab.com/hashicorp-demo/hashicorp//stack"
	ami = "ami-0425934b4a58f66e9"
	instance_type = "t2.micro"
	environment = "${module.network.environment}"
	region = "${module.network.region}"
	azs = "${module.network.azs}"
	vpcid = "${module.network.vpcid}"
	private_subnet_ids = "${module.network.private_subnet_ids}"
	public_subnet_ids = "${module.network.public_subnet_ids}"
	public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDIxB68bcMtA4kqVJwyVtEwwQoALzM09pypxI7dZrwLtpmMyeywHnHq7WIbanrgsD2PVeVfbgXESGvBJfRS/kYfU6kkAYb/Z64/9uDp5ARKK3Jta8Hw6tmYDjWOBzm+IFtevkGptHs7gBzuLLck2H2TeTlzkzanZF/a9L3VP/KgwbOiK641aIb921Cfq5oj07VJhl6cx8LLDpzo+gONBaIx9/OJwOJe9h85BKkQTcWXSnsIbCuITFU/TjYqNoWeDWuD4ctJ5ENWSNFPJSlrwDHejrY6KCnyRCAELWFgzyDHyR60kSwLOBPom5cgqVYYIB8opSlWJ/Uwto5H/bVT0x34WyprtLSyLnoyFQC/+/9qsG50IYHWDitLxoOR5QG/2uJKxLSrZRrFDuMLdWq1hyGTFdvGkl85+wmwZLmavzla9hlpw4iJLDuCpl8uH7ePN7/m9Q5DSvz7eTghLeJvPnKiocJ45pwy1mN0MF9UzKvh80FJKzlKjWjSOtosrrx7WC3aEg4przb5i+iwsY7vc0AmRFFfj7AIysvgx7oueY/vRAz6ox5tFK+9+P0+fXS3g2eTcMCefJt6lsQzFCKKmF8ZxMof2z9O++7zrragTc4kxsrLC/30/44Krn7CIosWivVqraP3gLza8ebUl8ctJF83W+m6oZTMKI/dnRycsuF87w=="
}

output "elb_public_name" {
  value = "${module.stack.elb_public_name}"
}